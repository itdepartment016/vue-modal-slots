# vue-modal-slots

Добавлен 
"sass": "^1.38.0",
"sass-loader": "^10.1.1" - причем sass-loader понижен до 10.1.1, для совместимости с vue/cli

Создан vue.config.js

для использования SCSS в компоненте использовать <style lang="scss">



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).



